<?php

function commerce_simple_settings_form($form, $form_state) {
  $node_types = array();
  $fields = field_info_fields();
  // We don't use commerce_product_reference_node_types() because we also need
  // to check the cardinality.
  foreach ($fields as $field_name => $field) {
    if ($field['type'] == 'commerce_product_reference' &&
        $field['cardinality'] == 1 &&
        !empty($field['bundles']['node'])) {
      foreach ($field['bundles']['node'] as $type) {
        $node_types[$type] = $field_name;
      }
    }
  }
  if (empty($node_types)) {
    drupal_set_message(t('You need to create at least one content type with a product reference field of cardinality 1.'), 'error');
    return;
  }
  // For each of the node types we need to know which product type they
  // can reference.
  $product_type_machine_names = array_keys(commerce_product_types());
  $displays = array();
  $instances = field_info_instances('node');
  foreach ($node_types as $node_type => $field_name) {
    $instance = $instances[$node_type][$field_name];
    if (!($product_types = array_filter($instance['settings']['referenceable_types']))) {
      $product_types = $product_type_machine_names;
    }
    foreach ($product_types as $product_type) {
      $displays[$product_type][] = $node_type;
    }
  }
  $form['product_types'] = array(
    '#tree' => TRUE,
  );
  $defaults = variable_get('commerce_simple_product_type_map', array());
  $product_types = commerce_product_types();
  $node_names = node_type_get_names();
  foreach ($displays as $product_type => $node_types) {
    $options = array_intersect_key($node_names, array_flip($node_types));
    $form['product_types'][$product_type] = array(
      '#title' => $product_types[$product_type]['name'],
      '#type' => 'radios',
      '#required' => FALSE,
      '#options' => array('' => 'N/A') + $options,
      '#default_value' => empty($defaults[$product_type]) ? '' : $defaults[$product_type],
    );
  }
  $form['actions'] = array('#type' => 'actions');
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

function commerce_simple_settings_form_submit($form, $form_state) {
  variable_set('commerce_simple_product_type_map', $form_state['values']['product_types']);
  drupal_set_message(t('The configuration options have been saved.'));
}
