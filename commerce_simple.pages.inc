<?php

function commerce_simple_node_page_edit($node) {
  $map = variable_get('commerce_simple_product_type_map', array());
  if (!in_array($node->type, $map)) {
    module_load_include('inc', 'node', 'node.pages');
    return node_page_edit($node);
  }
  module_load_include('inc', 'commerce_product_ui', 'includes/commerce_product_ui.products');
  $field_name = commerce_simple_product_reference_field_name($node->type);
  $items = field_get_items('node', $node, $field_name);
  // See $items['admin/commerce/products/%commerce_product'] in
  // commerce_product_ui_menu().
  $product = commerce_product_load($items[0]['product_id']);
  $title = commerce_product_ui_product_title($product);
  drupal_set_title($title);
  return commerce_product_ui_product_form_wrapper($product);
}
