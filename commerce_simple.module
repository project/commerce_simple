<?php

/**
 * Implements hook_menu().
 */
function commerce_simple_menu() {
  $items['admin/commerce/products/displays'] = array(
    'title' => 'Node displays',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_simple_settings_form'),
    'access arguments' => array('administer product types'),
    'file' => 'commerce_simple.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );
  return $items;
}

/**
 * Implements hook_menu_alter().
 */
function commerce_simple_menu_alter(&$items) {
  $items['node/%node/edit'] = array(
    'page callback' => 'commerce_simple_node_page_edit',
    'file' => 'commerce_simple.pages.inc',
    'file path' => drupal_get_path('module', 'commerce_simple'),
  ) + $items['node/%node/edit'];
}

/**
 * Implements hook_commerce_product_insert().
 *
 * Creates a new product display for each created product if the
 * product type has been linked to a default display.
 */
function commerce_simple_commerce_product_insert($product) {
  $map = variable_get('commerce_simple_product_type_map', array());
  if (!empty($map[$product->type])) {
    module_load_include('inc', 'node', 'node.pages');
    $display = new stdClass;
    $display->is_new = TRUE;
    $display->type = $map[$product->type];
    $display->title = $product->title;
    $display->status = $product->status;
    $display->language = LANGUAGE_NONE;
    node_object_prepare($display);
    $field_name = commerce_simple_product_reference_field_name($map[$product->type]);
    $display->{$field_name}[LANGUAGE_NONE][0]['product_id'] = $product->product_id;
    node_save($display);
  }
}

/**
 * Returns the field name of the commerce product reference field
 * for a given node type.
 */
function commerce_simple_product_reference_field_name($node_type) {
  $fields = field_info_fields();
  $fields = array_filter($fields, function($field) use ($node_type) {
    return $field['type'] == 'commerce_product_reference' &&
      $field['cardinality'] == 1 &&
      !empty($field['bundles']['node']) &&
      in_array($node_type, $field['bundles']['node']);
  });
  $field_names = array_keys($fields);
  return $field_names[0];
}

/**
 * Implements hook_commerce_product_update().
 */
function commerce_simple_commerce_product_update($product) {
  $map = variable_get('commerce_simple_product_type_map', array());
  if (empty($map[$product->type])) {
    return;
  }
  // Get the node that references this product. There should be exactly 1.
  $field_name = commerce_simple_product_reference_field_name($map[$product->type]);
  $query = new EntityFieldQuery();
  $result = $query->entityCondition('entity_type', 'node')
    ->fieldCondition($field_name, 'product_id', $product->product_id, '=')
    ->execute();
  $node = reset($result['node']);
  $node = node_load($node->nid);
  $node->status = $product->status;
  $node->title = $product->title;
  node_save($node);
}
